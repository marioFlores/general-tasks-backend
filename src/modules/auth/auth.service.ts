import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt/dist/jwt.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthResponse, SigninDto } from './dto/signin.dto';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import { RegisterDto } from './dto';

@Injectable()
export class AuthService {
    constructor(private readonly db: PrismaService, private readonly jwt: JwtService, private readonly usersService: UsersService){}

    async login(data: SigninDto): Promise<AuthResponse> {
      const { username, password } = data;

      const user = await this.db.users.findUnique({
        where: { username },
      });
      
      if (!user) {
        throw new NotFoundException("Not found user");
      } 
      
      const passwordValid = await bcrypt.compare(password, user.password);
  
      if (!passwordValid) {
        throw new UnauthorizedException('invalid password');
      }
      
      delete user.password;
  
      return {
        token: this.jwt.sign({ username }),
        user,
      };
    }

    async register(data: RegisterDto): Promise<AuthResponse> {
      const user = await this.usersService.createUser(data);
      return {
        token: this.jwt.sign({ username: user.username }),
        user,
      };
    }
}
