import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto';
import { AuthResponse, SigninDto } from './dto/signin.dto';

@Controller('auth')
export class AuthController {
    constructor(private readonly service: AuthService){}

    @Post('login')
    login(@Body() data: SigninDto): Promise<AuthResponse> {
        return this.service.login(data);
    }

    @Post('register')
        register(@Body() data: RegisterDto): Promise<AuthResponse> {
        return this.service.register(data);
    }
}
