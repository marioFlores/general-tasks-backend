import { type } from '@prisma/client';
import { IsNotEmpty, IsString, Length } from 'class-validator';
export class RegisterDto{

    @IsNotEmpty()
    @IsString()
    readonly name: string;

    @IsNotEmpty()
    @IsString()
    readonly last_name: string;

    @IsNotEmpty()
    @IsString()
    @Length(3, 30)
    readonly username: string;

    @IsNotEmpty()
    @IsString()
    @Length(6, 30)
    readonly email: string;
    
    @IsNotEmpty()
    @IsString()
    @Length(1, 50)
    readonly password: string;

    @IsNotEmpty()
    readonly type: type;

}