import { IsNotEmpty, IsString, Length } from 'class-validator';
import { users } from '@prisma/client';

export class SigninDto{
    
    @IsNotEmpty()
    @IsString()
    @Length(3, 30)
    readonly username: string;
      
    @IsNotEmpty()
    @IsString()
    @Length(6, 30)
    readonly password: string;
}

export class AuthResponse {
  token: string;
  user: users;
}