import { Body, Controller, Get, Param, ParseIntPipe, Patch, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ReadUserDto } from './dto/read-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersService } from './users.service';

@UseGuards(AuthGuard('jwt'))
@Controller('users')
export class UsersController {
    constructor(private readonly _userService: UsersService){}

    @Get('all')
    async getUsers(): Promise<ReadUserDto[]>{
        return await this._userService.getUsers();
    }

    @Get('findBy/:id')
    async getUser(@Param('id', ParseIntPipe) id: number): Promise<ReadUserDto | null>{
        return await this._userService.getUser(id); 
    }

    @Put('update/:id')
    async updateUser(@Body() user: UpdateUserDto, @Param('id', ParseIntPipe) id: number){
        const UserExists =  await this._userService.updateUser(user, id);
    }

    @Patch('delete/:id')
    async deleteUser(@Param('id', ParseIntPipe) id: number){
        return await this._userService.deleteUser(id);
    }
}
