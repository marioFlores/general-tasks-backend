import { status, type } from '@prisma/client';
import {IsNotEmpty} from 'class-validator';

export class CreateUserDto{

    @IsNotEmpty()
    readonly name: string;

    @IsNotEmpty()
    readonly last_name: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;

    @IsNotEmpty()
    readonly type: type;

    @IsNotEmpty()
    readonly username: string
}