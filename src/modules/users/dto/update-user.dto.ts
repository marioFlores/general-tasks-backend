import { type, status } from "@prisma/client";
import { IsNotEmpty } from "class-validator";

export class UpdateUserDto{
    @IsNotEmpty()
    readonly id: string;

    @IsNotEmpty()
    readonly name: string;

    @IsNotEmpty()
    readonly last_name: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;

    @IsNotEmpty()
    readonly type: type;

    @IsNotEmpty()
    readonly status: status; 
}