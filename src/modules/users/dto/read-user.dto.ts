import { status, type } from '@prisma/client';
import {IsNotEmpty} from 'class-validator';

export class ReadUserDto{
    @IsNotEmpty()
    readonly id: number;

    @IsNotEmpty()
    readonly name: string;

    @IsNotEmpty()
    readonly last_name: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly type: type;

    @IsNotEmpty()
    readonly date_create: Date;

    @IsNotEmpty()
    readonly status: status;

    @IsNotEmpty()
    readonly username: string;
}