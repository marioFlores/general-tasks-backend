import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { genSalt, hash } from 'bcryptjs';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ReadUserDto } from './dto/read-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { users } from '@prisma/client';
import { IsEmail } from 'class-validator';

@Injectable()
export class UsersService {
    constructor(private readonly _prismaService: PrismaService){
    }

    async getUser(idUser: number): Promise<ReadUserDto> { 
        const user = await this._prismaService.users.findUnique({
            where:{
                id: idUser
            }
        });
    
        if (!user) {
          throw new NotFoundException();
        }
    
        delete user.password;

        return user;
    }

    async getUsers(): Promise<ReadUserDto[]> {
        const users= await this._prismaService.users.findMany();
    
        if (!users) {
          throw new NotFoundException();
        }

        users.forEach(user=>{
            delete user.password;
        });

        return users;
    }

    async createUser(data: CreateUserDto): Promise<users>{        
        const existingUsername = await this._prismaService.users.findUnique({
            where:{
                username: data.username
            },
        });

        const existingEmail = await this._prismaService.users.findUnique({
            where:{
                email: data.email
            },
        });

        if (existingUsername) {
            throw new ConflictException('username already exists');
        };

        if (existingEmail) {
            throw new ConflictException('email already exists');
        };

        const salt = await genSalt(10);

        const hashedPassword = await hash(data.password, salt);

        const user = await this._prismaService.users.create({
            data: {
                ...data,
                password: hashedPassword,
            },
        });
      
        delete user.password;

        return user;
    }

    async updateUser(data: UpdateUserDto, id: number){
        const userExists =  await this._prismaService.users.findUnique({
            where:{
                id: id
            }
        });

        if(!userExists){
            throw new NotFoundException();
        } 

        const user = await this._prismaService.users.update({
            where: {
                id: id
            },
            data:{
                name: data.name,
                last_name: data.last_name,
                email: data.email,
                password: data.password,
                type: data.type,
                status: data.status
            }
        });

        delete user.password;

        return user;
    }

    async deleteUser(id: number){
        const UserExists =  await this._prismaService.users.findUnique({
            where:{
                id: id
            }
        });

        if(!UserExists){
            throw new NotFoundException();
        }

        const user = await this._prismaService.users.update({
            where:{
                id: id
            },
            data: {
                status: "DISABLE"
            }
        });

        return user;
    }
}
