import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { tasks } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { ReadTaskDto } from './dto/read-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {

    constructor(private readonly _prismaService: PrismaService){
    }

    async getTask(id: number): Promise<ReadTaskDto> {
        const task = this._prismaService.tasks.findUnique({
            where:{
                id: id
            }
        });
    
        if (!task) {
          throw new NotFoundException();
        }

        return task;
    }

    async getTasks(): Promise<ReadTaskDto[]> {
        const tasks= await this._prismaService.tasks.findMany();
    
        if (!tasks) {
          throw new NotFoundException();
        }

        return tasks;
    }

    async getTasksByUser(id: number){
        const tasks = await this._prismaService.tasks.findMany({
            where:{
                id_user: id
            }
        });

        if (!tasks) {
            throw new NotFoundException();
        }
  
        return tasks;
    }

    async getTasksStatus(state: boolean): Promise<tasks[]>{
        const tasks = await this._prismaService.tasks.findMany({
            where:{
                status: state
            }
        });

        if (!tasks) {
            throw new NotFoundException();
        }
  
        return tasks;
    }

    async createTask(data: CreateTaskDto): Promise<tasks>{
        let dateFormat = Date.parse(data.to_date);
        let date = new Date(dateFormat);        
        const task = await this._prismaService.tasks.create({
            data:{
                title: data.title,
                description: data.description,
                to_date: date,
                users:{
                    connect: {
                        id: data.id_user
                    },
                }
            }
        });

        return task;
    }

    async updateTask(data: UpdateTaskDto, id: number){
        let dateFormat = Date.parse(data.to_date);
        let date = new Date(dateFormat);
        
        const taskExists =  await this._prismaService.tasks.findUnique({
            where:{
                id: id
            }
        });

        if(!taskExists){
            throw new NotFoundException("task not found");
        }

        const task = await this._prismaService.tasks.update({
            where: {
                id: data.id,
            },
            data:{
                title: data.title,
                description: data.description,
                to_date: date
            }
        });

        return task;
    }

    async changeStatusTask(id: number){

        const taskExists =  await this._prismaService.tasks.findUnique({
            where:{
                id: id
            }
        });

        if(!taskExists){
            throw new NotFoundException('Task not found');
        }

        const task = await this._prismaService.tasks.update({
            where: {
                id: id,
            },
            data:{
                status: true
            }
        });

        return task;
    }

    async deleteTask(id: number){
        const taskExists =  await this._prismaService.tasks.findUnique({
            where:{
                id: id
            }
        });

        if(!taskExists){
            throw new NotFoundException();
        }

        const task = await this._prismaService.tasks.delete({
            where: {
                id: id
            }
        });

        return task;
    }
}
