import { Body, Controller, Delete, Get, Param, ParseBoolPipe, ParseIntPipe, Post, Put } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { ReadTaskDto } from './dto/read-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
    constructor(private readonly _taskService: TasksService){}

    @Get('all')
    async getTasks(): Promise<ReadTaskDto[]>{
        return await this._taskService.getTasks();
    }

    @Get('findBy/:id')
    async getTask(@Param('id', ParseIntPipe) id: number): Promise<ReadTaskDto>{
        return await this._taskService.getTask(id);
    }

    @Get('findByStatus/:state')
    async getTasksByStatus(@Param('state', ParseBoolPipe) state: boolean): Promise<ReadTaskDto[]>{
        return await this._taskService.getTasksStatus(state);
    }

    @Get('findByUser/:id')
    async getTasksByUser(@Param('id', ParseIntPipe) id: number): Promise<ReadTaskDto[]>{
        return await this._taskService.getTasksByUser(id);
    }

    @Post('create')
    async createTask(@Body() task: CreateTaskDto){
        return await this._taskService.createTask(task);
    }

    @Put('update/:id')
    async updateTask(@Body() task: UpdateTaskDto, @Param('id', ParseIntPipe) id: number){
        return await this._taskService.updateTask(task, id);
    }

    @Put('changeStatus/:id')
    async changeStatusTask(@Param('id', ParseIntPipe) id: number){
        return await this._taskService.changeStatusTask(id);
    }

    @Delete('delete/:id')
    async deleteTask(@Param('id', ParseIntPipe) id: number){
        return await this._taskService.deleteTask(id);
    }
}
