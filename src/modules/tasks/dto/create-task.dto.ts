import {IsNotEmpty} from 'class-validator';

export class CreateTaskDto{

    @IsNotEmpty()
    readonly id_user: number;

    @IsNotEmpty()
    readonly title: string;

    @IsNotEmpty()
    readonly description: string;   

    @IsNotEmpty()
    readonly to_date: string;

}