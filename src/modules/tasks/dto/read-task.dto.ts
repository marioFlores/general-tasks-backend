import {IsNotEmpty} from 'class-validator';

export class ReadTaskDto{
    
    @IsNotEmpty()
    readonly id: number;

    @IsNotEmpty()
    readonly id_user: number;

    @IsNotEmpty()
    readonly date_create: Date;

    @IsNotEmpty()
    readonly title: string;

    @IsNotEmpty()
    readonly description: string;   

    @IsNotEmpty()
    readonly status: boolean;   

    @IsNotEmpty()
    readonly to_date: Date;

}