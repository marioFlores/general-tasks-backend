import { User } from "./User";

export class Task{
    id: number;
    id_user: User;
    date_create: Date;
    title: string;   
    description: string;
    status: boolean
    to_date: Date;
}