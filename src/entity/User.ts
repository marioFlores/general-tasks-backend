import { type, status } from '@prisma/client';
export class User{
    id: number;
    name: string;    
    last_name: string;  
    email: string;
    password: string;
    type: type;
    status: status;
    username: string;   
}