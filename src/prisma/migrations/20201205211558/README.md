# Migration `20201205211558`

This migration has been generated by Mario Flores at 12/5/2020, 3:15:58 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE UNIQUE INDEX "users.username_unique" ON "users"("username")
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201205080701..20201205211558
--- datamodel.dml
+++ datamodel.dml
@@ -3,9 +3,9 @@
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model tasks {
   id          Int      @id @default(autoincrement())
@@ -26,9 +26,9 @@
   password    String
   type        type
   date_create DateTime @default(now())
   status      status   @default(ENABLE)
-  username    String?
+  username    String?  @unique
   tasks       tasks[]
 }
 enum status {
```


