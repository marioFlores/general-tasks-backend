import { Controller, Get, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { SimpleGuard } from './modules/auth/simple.guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @UseGuards(SimpleGuard)
  getHello(): string {
    return this.appService.getHello();
  }
}
